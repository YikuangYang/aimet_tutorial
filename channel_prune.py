import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--root', type=str, required=True)
parser.add_argument('--epochs', type=int, default=1)
parser.add_argument('--cuda', action='store_true', default=False)

args = parser.parse_args()

# 1. Loading and normalizing CIFAR10
import torch
import torchvision
import torchvision.transforms as transforms

transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

trainset = torchvision.datasets.CIFAR10(root=args.root, train=True,
                                        download=True, transform=transform)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=4,
                                          shuffle=True, num_workers=2)

testset = torchvision.datasets.CIFAR10(root=args.root, train=False,
                                       download=True, transform=transform)
testloader = torch.utils.data.DataLoader(testset, batch_size=4,
                                         shuffle=False, num_workers=2)

classes = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck')


# 2. Define a Convolutional Neural Network
import torch.nn as nn
import torch.nn.functional as F

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 16 * 5 * 5)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

net = Net()
if args.cuda:
    net = net.cuda()

# 3. Define a Loss function and optimizer
import torch.optim as optim

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)
num_epochs = args.epochs

# 4. Train the network
def train(model, trainloader, criterion, optimizer, num_epochs, use_cuda=args.cuda):
    for epoch in range(num_epochs):  # loop over the dataset multiple times

        running_loss = 0.0
        acc = 0.0
        for i, data in enumerate(trainloader, 0):
            # get the inputs; data is a list of [inputs, labels]
            inputs, labels = data[0], data[1]
            if use_cuda:
                inputs = inputs.cuda()
                labels = labels.cuda()

            # zero the parameter gradients
            optimizer.zero_grad()

            # forward + backward + optimize
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            preds = outputs.argmax(dim=1)
            acc += (preds == labels).sum()

            # print statistics
            running_loss += loss.item()
            if i % 2000 == 1999:    # print every 2000 mini-batches
                print('[%d, %5d] loss: %.3f acc: %.3f' %
                    (epoch + 1, i + 1, running_loss / 2000, float(acc) / (2000 * inputs.shape[0])))
                running_loss = 0.0
                acc = 0.0

train(net, trainloader, criterion, optimizer, num_epochs, args.cuda)
print('Finished Training')

# 5. Test the model
def evaluate(model, eval_iterations=None, use_cuda=args.cuda):
    with torch.no_grad():
        acc = 0
        num = 0
        for i, data in enumerate(testloader, 0):
            if eval_iterations is not None and i >= eval_iterations:
                break
            # get the inputs; data is a list of [inputs, labels]
            inputs, labels = data[0], data[1]
            if use_cuda:
                inputs = inputs.cuda()
                labels = labels.cuda()
            outputs = model(inputs)
            preds = outputs.argmax(dim=1)
            acc += (preds == labels).sum()
            num += inputs.shape[0]
        return float(acc) / num
acc = evaluate(net)
print('test acc %.3f' % (acc,))

# train script credits to https://pytorch.org/tutorials/beginner/blitz/cifar10_tutorial.html

# 6. Channel Pruning

# Compression-related imports
from decimal import Decimal
from aimet_common.defs import CostMetric, CompressionScheme, GreedySelectionParameters, RankSelectScheme
from aimet_torch.defs import WeightSvdParameters, SpatialSvdParameters, ChannelPruningParameters
from aimet_torch.compress import ModelCompressor

input_shape = (1, 3, 32, 32)
savepath = './cp.onnx'
savepath_m = 'cp.pt'
comp_ratio = 0.5

from functools import partial
train_wrapper = partial(train, trainloader=trainloader, criterion=criterion, 
                        optimizer=optimizer, num_epochs=num_epochs, use_cuda=args.cuda)

class Trainer:

    def __init__(self, train_wrapper):
        self._layer_db = []
        self.train_wrapper = train_wrapper

    def train_model(self, model, layer, train_flag=True):
        """
        Trains a model
        :param model: Model to be trained
        :param layer: layer which has to be fine tuned
        :param train_flag: Default: True. If ture the model gets trained
        :return:
        """
        if train_flag:
            # start to train
            self.train_wrapper(model)
        self._layer_db.append(layer)

def channel_pruning_auto_mode(model, input_shape, savepath, comp_ratio, trainloader, eval_callback, trainer):

    # Specify the necessary parameters
    greedy_params = GreedySelectionParameters(target_comp_ratio=Decimal(comp_ratio),
                                              num_comp_ratio_candidates=10)
    auto_params = ChannelPruningParameters.AutoModeParams(greedy_params, 
                                                          # the first layer must be ignored
                                                          modules_to_ignore=[model.conv1])

    params = ChannelPruningParameters(data_loader=trainloader,
                                      num_reconstruction_samples=1000,
                                      allow_custom_downsample_ops=True,
                                      mode=ChannelPruningParameters.Mode.auto,
                                      params=auto_params)

    # Single call to compress the model

    results = ModelCompressor.compress_model(model,
                                             eval_callback=eval_callback,
                                             eval_iterations=1000,
                                             input_shape=input_shape,
                                             compress_scheme=CompressionScheme.channel_pruning,
                                             cost_metric=CostMetric.mac,
                                             parameters=params, 
                                             trainer=trainer)

    compressed_model, stats = results
    print(compressed_model)
    print(stats)     # Stats object can be pretty-printed easily
    rand_inp = torch.rand(input_shape)
    if args.cuda:
        rand_inp = rand_inp.cuda()
    torch.onnx.export(compressed_model, rand_inp, savepath)
    torch.save(compressed_model.state_dict(), savepath_m)

trainer = Trainer(train_wrapper)
channel_pruning_auto_mode(net, (1,3,32,32), savepath, comp_ratio, trainloader, evaluate, trainer)
