import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--root', type=str, required=True)
parser.add_argument('--epochs', type=int, default=1)
parser.add_argument('--cuda', action='store_true', default=False)

# 1. Loading and normalizing CIFAR10
import torch
import torchvision
import torchvision.transforms as transforms



# 2. Define a Convolutional Neural Network
import torch.nn as nn
import torch.nn.functional as F

# 3. Define a Loss function and optimizer
import torch.optim as optim

# 6. quantization simulation
from aimet_torch.quantsim import QuantParams
from aimet_torch.quantsim import QuantizationSimModel
from functools import partial
